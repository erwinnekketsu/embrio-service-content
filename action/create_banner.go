package action

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-content.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-content.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-content.git/transport/grpc/proto/content"
)

type CreateBanner struct {
	builder *builder.Grpc
	repo    repo.IBanner
}

func NewCreateBanner() *CreateBanner {
	return &CreateBanner{
		builder: builder.NewGrpc(),
		repo:    repo.NewBanner(),
	}
}

func (h *CreateBanner) Handler(ctx context.Context, req *pb.CreateBannerRequest) error {
	dataBanner := h.builder.CreateBannerRequest(req)
	return h.repo.CreateUpdateBanner(ctx, dataBanner)
}
