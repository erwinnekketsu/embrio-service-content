package action

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-content.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-content.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-content.git/transport/grpc/proto/content"
)

type CreateNews struct {
	builder *builder.Grpc
	repo    repo.INews
}

func NewCreateNews() *CreateNews {
	return &CreateNews{
		builder: builder.NewGrpc(),
		repo:    repo.NewNews(),
	}
}

func (h *CreateNews) Handler(ctx context.Context, req *pb.CreateNewsRequest) error {
	dataNews := h.builder.CreateNewsRequest(req)
	return h.repo.CreateUpdateNews(ctx, dataNews)
}
