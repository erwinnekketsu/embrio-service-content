package action

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-content.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-content.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-content.git/transport/grpc/proto/content"
)

type CreateWa struct {
	builder *builder.Grpc
	repo    repo.IWa
}

func NewCreateWa() *CreateWa {
	return &CreateWa{
		builder: builder.NewGrpc(),
		repo:    repo.NewWa(),
	}
}

func (h *CreateWa) Handler(ctx context.Context, req *pb.CreateWaRequest) error {
	dataWa := h.builder.CreateWaRequest(req)
	return h.repo.CreateUpdateWa(ctx, dataWa)
}
