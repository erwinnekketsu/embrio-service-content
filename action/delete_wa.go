package action

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-content.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-content.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-content.git/transport/grpc/proto/content"
)

type DeleteWa struct {
	builder *builder.Grpc
	repo    repo.IWa
}

func NewDeleteWa() *DeleteWa {
	return &DeleteWa{
		builder: builder.NewGrpc(),
		repo:    repo.NewWa(),
	}
}

func (h *DeleteWa) Handler(ctx context.Context, req *pb.DeleteWaRequest) error {
	err := h.repo.DeleteWa(ctx, req.Id)
	if err != nil {
		return err
	}
	return nil
}
