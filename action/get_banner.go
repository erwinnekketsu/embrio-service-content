package action

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-content.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-content.git/entity"
	"gitlab.com/erwinnekketsu/embrio-service-content.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-content.git/transport/grpc/proto/content"
	// "gitlab.com/erwinnekketsu/embrio-service-content.git/util"
)

type GetBanner struct {
	builder *builder.Grpc
	repo    repo.IBanner
}

func NewGetBanner() *GetBanner {
	return &GetBanner{
		builder: builder.NewGrpc(),
		repo:    repo.NewBanner(),
	}
}

func (h *GetBanner) Handler(ctx context.Context, req *pb.GetBannerRequest) (res entity.Banner, err error) {
	return h.repo.GetBannerById(ctx, int(req.Id))
}
