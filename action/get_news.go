package action

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-content.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-content.git/entity"
	"gitlab.com/erwinnekketsu/embrio-service-content.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-content.git/transport/grpc/proto/content"
	// "gitlab.com/erwinnekketsu/embrio-service-content.git/util"
)

type GetNews struct {
	builder *builder.Grpc
	repo    repo.INews
}

func NewGetNews() *GetNews {
	return &GetNews{
		builder: builder.NewGrpc(),
		repo:    repo.NewNews(),
	}
}

func (h *GetNews) Handler(ctx context.Context, req *pb.GetNewsRequest) (res entity.News, err error) {
	return h.repo.GetNewsById(ctx, int(req.Id))
}
