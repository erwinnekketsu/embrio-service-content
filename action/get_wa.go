package action

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-content.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-content.git/entity"
	"gitlab.com/erwinnekketsu/embrio-service-content.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-content.git/transport/grpc/proto/content"
	// "gitlab.com/erwinnekketsu/embrio-service-content.git/util"
)

type GetWa struct {
	builder *builder.Grpc
	repo    repo.IWa
}

func NewGetWa() *GetWa {
	return &GetWa{
		builder: builder.NewGrpc(),
		repo:    repo.NewWa(),
	}
}

func (h *GetWa) Handler(ctx context.Context, req *pb.GetWaRequest) (res entity.Wa, err error) {
	return h.repo.GetWaById(ctx, int(req.Id))
}
