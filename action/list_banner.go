package action

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-content.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-content.git/entity"
	"gitlab.com/erwinnekketsu/embrio-service-content.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-content.git/transport/grpc/proto/content"
	"gitlab.com/erwinnekketsu/embrio-service-content.git/util"
)

type ListBanner struct {
	builder *builder.Grpc
	repo    repo.IBanner
}

func NewListBanner() *ListBanner {
	return &ListBanner{
		builder: builder.NewGrpc(),
		repo:    repo.NewBanner(),
	}
}

func (h *ListBanner) Handler(ctx context.Context, req *pb.ListBannerRequest) (res []entity.Banner, pagintaion *util.Pagination, err error) {
	return h.repo.GetBanner(ctx, int(req.Page), int(req.Limit), int(req.Status))
}
