package action

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-content.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-content.git/entity"
	"gitlab.com/erwinnekketsu/embrio-service-content.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-content.git/transport/grpc/proto/content"
	"gitlab.com/erwinnekketsu/embrio-service-content.git/util"
)

type ListNews struct {
	builder *builder.Grpc
	repo    repo.INews
}

func NewListNews() *ListNews {
	return &ListNews{
		builder: builder.NewGrpc(),
		repo:    repo.NewNews(),
	}
}

func (h *ListNews) Handler(ctx context.Context, req *pb.ListNewsRequest) (res []entity.News, pagintaion *util.Pagination, err error) {
	return h.repo.GetNews(ctx, int(req.Page), int(req.Limit), int(req.Status))
}
