package action

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-content.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-content.git/entity"
	"gitlab.com/erwinnekketsu/embrio-service-content.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-content.git/transport/grpc/proto/content"
	"gitlab.com/erwinnekketsu/embrio-service-content.git/util"
)

type ListWa struct {
	builder *builder.Grpc
	repo    repo.IWa
}

func NewListWa() *ListWa {
	return &ListWa{
		builder: builder.NewGrpc(),
		repo:    repo.NewWa(),
	}
}

func (h *ListWa) Handler(ctx context.Context, req *pb.ListWaRequest) (res []entity.Wa, pagintaion *util.Pagination, err error) {
	return h.repo.GetWa(ctx, int(req.Page), int(req.Limit))
}
