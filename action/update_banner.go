package action

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-content.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-content.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-content.git/transport/grpc/proto/content"
)

type UpdateBanner struct {
	builder *builder.Grpc
	repo    repo.IBanner
}

func NewUpdateBanner() *UpdateBanner {
	return &UpdateBanner{
		builder: builder.NewGrpc(),
		repo:    repo.NewBanner(),
	}
}

func (h *UpdateBanner) Handler(ctx context.Context, req *pb.UpdateBannerRequest) error {
	dataBanner := h.builder.UpdateBannerRequest(req)
	return h.repo.CreateUpdateBanner(ctx, dataBanner)
}
