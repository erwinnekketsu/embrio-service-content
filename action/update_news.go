package action

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-content.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-content.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-content.git/transport/grpc/proto/content"
)

type UpdateNews struct {
	builder *builder.Grpc
	repo    repo.INews
}

func NewUpdateNews() *UpdateNews {
	return &UpdateNews{
		builder: builder.NewGrpc(),
		repo:    repo.NewNews(),
	}
}

func (h *UpdateNews) Handler(ctx context.Context, req *pb.UpdateNewsRequest) error {
	dataNews := h.builder.UpdateNewsRequest(req)
	return h.repo.CreateUpdateNews(ctx, dataNews)
}
