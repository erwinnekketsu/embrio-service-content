package action

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-content.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-content.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-content.git/transport/grpc/proto/content"
)

type UpdateWa struct {
	builder *builder.Grpc
	repo    repo.IWa
}

func NewUpdateWa() *UpdateWa {
	return &UpdateWa{
		builder: builder.NewGrpc(),
		repo:    repo.NewWa(),
	}
}

func (h *UpdateWa) Handler(ctx context.Context, req *pb.UpdateWaRequest) error {
	dataWa := h.builder.UpdateWaRequest(req)
	return h.repo.CreateUpdateWa(ctx, dataWa)
}
