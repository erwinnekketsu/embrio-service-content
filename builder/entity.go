package builder

import (
	"context"
	"log"
	"os"
	"time"

	"github.com/cloudinary/cloudinary-go"
	"github.com/cloudinary/cloudinary-go/api/uploader"
	"gitlab.com/erwinnekketsu/embrio-service-content.git/entity"
	cld "gitlab.com/erwinnekketsu/embrio-service-content.git/repo/cloudinary"
	"gitlab.com/erwinnekketsu/embrio-service-content.git/repo/mysql"
	"gitlab.com/erwinnekketsu/embrio-service-content.git/util"
)

type Entity struct {
	cloudinary *cloudinary.Cloudinary
}

func NewEntity() *Entity {
	return &Entity{
		cloudinary: cld.Cld,
	}
}

func (e *Entity) CreateUpdateNews(ctx context.Context, req entity.News) (data mysql.News) {
	if req.ID != 0 {
		data.ID = req.ID
	}
	randomName, _ := util.GenerateRandomString(5)
	dateNow := time.Now().UTC()
	news := "news_" + randomName

	go func() {
		ctx = context.Background()
		_, err := e.cloudinary.Upload.Upload(ctx, req.Thumbnail, uploader.UploadParams{PublicID: news, Folder: "news"})
		if err != nil {
			log.Println("err")
			log.Println(err)
		}
	}()
	ImageURL := os.Getenv("cloudinary_image_url") + "/news/" + news + ".png"
	data.IdAdmin = req.IdAdmin
	data.Title = req.Title
	data.Content = req.Content
	data.Status = req.Status
	data.Thumbnail = ImageURL
	if req.ID != 0 {
		data.UpdatedAt = &dateNow
	} else {
		data.CreatedAt = &dateNow
	}
	return data
}

func (e *Entity) GetNews(req mysql.News) (res entity.News) {
	res.ID = req.ID
	res.IdAdmin = req.IdAdmin
	res.Title = req.Title
	res.Content = req.Content
	res.Status = req.Status
	res.Thumbnail = req.Thumbnail

	if req.UpdatedAt != nil {
		res.Date = req.UpdatedAt.String()
	} else {
		res.Date = req.CreatedAt.String()
	}
	return res
}

func (e *Entity) CreateUpdateBanner(ctx context.Context, req entity.Banner) (data mysql.Banner) {
	if req.ID != 0 {
		data.ID = req.ID
	}
	randomName, _ := util.GenerateRandomString(5)
	dateNow := time.Now().UTC()
	banner := "banner_" + randomName

	go func() {
		ctx = context.Background()
		_, err := e.cloudinary.Upload.Upload(ctx, req.Image, uploader.UploadParams{PublicID: banner, Folder: "banner"})
		if err != nil {
			log.Println("err")
			log.Println(err)
		}
	}()
	ImageURL := os.Getenv("cloudinary_image_url") + "/banner/" + banner + ".png"
	data.IdAdmin = req.IdAdmin
	data.Name = req.Name
	data.Image = ImageURL
	data.Status = req.Status
	if req.ID != 0 {
		data.UpdatedAt = &dateNow
	} else {
		data.CreatedAt = &dateNow
	}
	return data
}

func (e *Entity) GetBanner(req mysql.Banner) (res entity.Banner) {
	res.ID = req.ID
	res.IdAdmin = req.IdAdmin
	res.Name = req.Name
	res.Image = req.Image
	res.Status = req.Status

	if req.UpdatedAt != nil {
		res.Date = req.UpdatedAt.String()
	} else {
		res.Date = req.CreatedAt.String()
	}
	return res
}

func (e *Entity) CreateUpdateWa(ctx context.Context, req entity.Wa) (data mysql.Wa) {
	if req.ID != 0 {
		data.ID = req.ID
	}
	randomName, _ := util.GenerateRandomString(5)
	dateNow := time.Now().UTC()
	wa := "wa-group_" + randomName

	go func() {
		ctx = context.Background()
		_, err := e.cloudinary.Upload.Upload(ctx, req.Image, uploader.UploadParams{PublicID: wa, Folder: "wa-group"})
		if err != nil {
			log.Println("err")
			log.Println(err)
		}
	}()
	ImageURL := os.Getenv("cloudinary_image_url") + "/wa-group/" + wa + ".png"
	data.Name = req.Name
	data.Image = ImageURL
	data.Link = req.Link
	data.Member = req.Member
	if req.ID != 0 {
		data.UpdatedAt = &dateNow
	} else {
		data.CreatedAt = &dateNow
	}
	return data
}

func (e *Entity) GetWa(req mysql.Wa) (res entity.Wa) {
	res.ID = req.ID
	res.Name = req.Name
	res.Image = req.Image
	res.Link = req.Link
	res.Member = req.Member

	if req.UpdatedAt != nil {
		res.Date = req.UpdatedAt.String()
	} else {
		res.Date = req.CreatedAt.String()
	}
	return res
}

func (e *Entity) DeleteWa(ctx context.Context, id int64) (data mysql.Wa) {
	data.ID = int32(id)
	return data
}
