package builder

import (
	"encoding/json"
	"log"

	"gitlab.com/erwinnekketsu/embrio-service-content.git/entity"
	pb "gitlab.com/erwinnekketsu/embrio-service-content.git/transport/grpc/proto/content"
	pbNotif "gitlab.com/erwinnekketsu/embrio-service-content.git/transport/grpc/proto/notification"
)

// Grpc struct grpc builder
type Grpc struct{}

// NewGrpc for initiate builder func
func NewGrpc() *Grpc {
	return &Grpc{}
}

func (g *Grpc) CreateNewsRequest(req *pb.CreateNewsRequest) (data entity.News) {
	data.IdAdmin = req.AdminId
	data.Title = req.Title
	data.Content = req.Content
	data.Thumbnail = req.Thumbnail
	data.Status = req.Status
	return data
}

func (g *Grpc) NewsNotifRequest(req entity.News) (data pbNotif.CreateNotificationRequest) {
	log.Println(req)
	data.Content = req.Content
	data.From = int32(req.IdAdmin)
	data.Title = req.Title

	return data
}

func (g *Grpc) UpdateNewsRequest(req *pb.UpdateNewsRequest) (data entity.News) {
	data.ID = req.Id
	data.IdAdmin = req.AdminId
	data.Title = req.Title
	data.Content = req.Content
	data.Thumbnail = req.Thumbnail
	data.Status = req.Status
	return data
}

// ListNewsResponse generate response
func (g *Grpc) ListNewsResponse(data []entity.News) (res []*pb.News) {
	bytes, _ := json.Marshal(&data)
	_ = json.Unmarshal(bytes, &res)

	return res
}

// GetNewsResponse generate response
func (g *Grpc) GetNewsResponse(data entity.News) (res *pb.News) {
	bytes, _ := json.Marshal(&data)
	_ = json.Unmarshal(bytes, &res)

	return res
}

func (g *Grpc) CreateBannerRequest(req *pb.CreateBannerRequest) (data entity.Banner) {
	data.IdAdmin = req.AdminId
	data.Name = req.Name
	data.Image = req.Image
	data.Status = req.Status
	return data
}

func (g *Grpc) UpdateBannerRequest(req *pb.UpdateBannerRequest) (data entity.Banner) {
	data.ID = req.Id
	data.IdAdmin = req.AdminId
	data.Name = req.Name
	data.Image = req.Image
	data.Status = req.Status
	return data
}

// ListBannerResponse generate response
func (g *Grpc) ListBannerResponse(data []entity.Banner) (res []*pb.Banner) {
	bytes, _ := json.Marshal(&data)
	_ = json.Unmarshal(bytes, &res)

	return res
}

// GetBannerResponse generate response
func (g *Grpc) GetBannerResponse(data entity.Banner) (res *pb.Banner) {
	bytes, _ := json.Marshal(&data)
	_ = json.Unmarshal(bytes, &res)

	return res
}

func (g *Grpc) CreateWaRequest(req *pb.CreateWaRequest) (data entity.Wa) {
	data.Name = req.Name
	data.Image = req.Image
	data.Link = req.Link
	data.Member = req.Member
	return data
}

func (g *Grpc) UpdateWaRequest(req *pb.UpdateWaRequest) (data entity.Wa) {
	data.ID = req.Id
	data.Name = req.Name
	data.Image = req.Image
	data.Link = req.Link
	data.Member = req.Member
	return data
}

// ListWaResponse generate response
func (g *Grpc) ListWaResponse(data []entity.Wa) (res []*pb.Wa) {
	bytes, _ := json.Marshal(&data)
	_ = json.Unmarshal(bytes, &res)

	return res
}

// GetWaResponse generate response
func (g *Grpc) GetWaResponse(data entity.Wa) (res *pb.Wa) {
	bytes, _ := json.Marshal(&data)
	_ = json.Unmarshal(bytes, &res)

	return res
}
