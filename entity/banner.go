package entity

type Banner struct {
	ID      int32  `json:"id"`
	IdAdmin int32  `json:"admin_id"`
	Name    string `json:"name"`
	Image   string `json:"image"`
	Status  int32  `json:"status"`
	Date    string `json:"date"`
}
