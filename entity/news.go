package entity

type News struct {
	ID        int32  `json:"id"`
	IdAdmin   int32  `json:"admin_id"`
	Title     string `json:"title"`
	Content   string `json:"content"`
	Status    int32  `json:"status"`
	Thumbnail string `json:"thumbnail"`
	Date      string `json:"date"`
}
