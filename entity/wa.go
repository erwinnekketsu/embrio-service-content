package entity

type Wa struct {
	ID     int32  `json:"id"`
	Name   string `json:"name"`
	Image  string `json:"image"`
	Link   string `json:"link"`
	Member int32  `json:"member"`
	Date   string `json:"date"`
}
