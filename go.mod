module gitlab.com/erwinnekketsu/embrio-service-content.git

go 1.16

require (
	github.com/cloudinary/cloudinary-go v1.2.0
	github.com/go-redis/redis/v8 v8.11.2
	github.com/golang/protobuf v1.5.2
	github.com/grpc-ecosystem/go-grpc-middleware v1.3.0
	github.com/jmoiron/sqlx v1.3.4
	github.com/joho/godotenv v1.3.0
	go.elastic.co/apm/module/apmgrpc v1.13.1
	go.elastic.co/apm/module/apmsql v1.13.1
	golang.org/x/crypto v0.0.0-20210711020723-a769d52b0f97
	google.golang.org/grpc v1.39.1
	google.golang.org/grpc/examples v0.0.0-20210806175644-574137db7de3 // indirect
	google.golang.org/protobuf v1.27.1
)
