package main

import (
	"log"

	"github.com/joho/godotenv"
	"gitlab.com/erwinnekketsu/embrio-service-content.git/repo/cloudinary"
	"gitlab.com/erwinnekketsu/embrio-service-content.git/repo/mysql"
	"gitlab.com/erwinnekketsu/embrio-service-content.git/transport/grpc"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
	mysql.InitCon()

	// redis.Init()

	cloudinary.Init()

	grpc.Run()
}
