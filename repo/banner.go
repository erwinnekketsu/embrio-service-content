package repo

import (
	"context"
	"log"

	"github.com/jmoiron/sqlx"
	"gitlab.com/erwinnekketsu/embrio-service-content.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-content.git/entity"
	"gitlab.com/erwinnekketsu/embrio-service-content.git/repo/mysql"
	"gitlab.com/erwinnekketsu/embrio-service-content.git/util"
)

type Banner struct {
	builder  *builder.Entity
	iMysql   mysql.IMysql
	embrioDB *sqlx.DB
}

func NewBanner() *Banner {
	return &Banner{
		builder:  builder.NewEntity(),
		iMysql:   mysql.NewClient(),
		embrioDB: mysql.EmbrioDB,
	}
}

func (p *Banner) CreateUpdateBanner(ctx context.Context, req entity.Banner) error {
	var query string
	if req.ID == 0 {
		query = mysql.QueryCreateBanner
	} else {
		if req.IdAdmin == 0 {
			query = mysql.QueryUpdateStatusBanner
		} else {
			query = mysql.QueryUpdateBanner
		}
	}

	dataBanner := p.builder.CreateUpdateBanner(ctx, req)
	_, err := p.iMysql.CreateOrUpdate(ctx, p.embrioDB, dataBanner, query)

	if err != nil {
		log.Println(err)
		return err
	}
	return nil
}

func (p *Banner) GetBanner(ctx context.Context, page int, limit int, status int) (data []entity.Banner, pagintaion *util.Pagination, err error) {
	query := &util.Query{}
	if status != 0 {
		query = &util.Query{
			Filter: map[string]interface{}{
				"status$eq": status,
			},
		}
		limit = 100
	}

	datas, paginations, err := p.iMysql.FindWithPaginationBanner(
		ctx,
		p.embrioDB,
		query,
		util.NewPagination(int32(page), int32(limit)),
		mysql.QueryGetBanner,
	)

	if err != nil {
		log.Println(err)
		return data, &util.Pagination{}, err
	}

	for _, v := range datas {
		date := ""
		if v.UpdatedAt != nil {
			date = v.UpdatedAt.String()
		} else {
			date = v.CreatedAt.String()
		}
		dataBanner := entity.Banner{
			ID:      v.ID,
			IdAdmin: v.IdAdmin,
			Name:    v.Name,
			Image:   v.Image,
			Status:  v.Status,
			Date:    date,
		}
		data = append(data, dataBanner)
	}

	return data, paginations, nil
}

func (p *Banner) GetBannerById(ctx context.Context, id int) (data entity.Banner, err error) {
	query := &util.Query{
		Filter: map[string]interface{}{
			"id$eq": id,
		},
	}
	var dataBanner mysql.Banner
	err = p.iMysql.Get(
		ctx,
		p.embrioDB,
		&dataBanner,
		query,
		mysql.QueryGetBanner,
	)

	if err != nil {
		log.Println(err)
		return data, err
	}

	data = p.builder.GetBanner(dataBanner)

	return data, nil
}
