package client

import (
	"context"
	"os"

	pb "gitlab.com/erwinnekketsu/embrio-service-content.git/transport/grpc/proto/notification"
	Grpc "gitlab.com/erwinnekketsu/embrio-service-content.git/util"
)

// CreateNotification client handler
func CreateNotification(ctx context.Context, req *pb.CreateNotificationRequest) (*pb.NoResponse, error) {
	conn := Grpc.Dial(os.Getenv("notification_grpc"))
	defer conn.Close()

	client := pb.NewNotificationClient(conn)

	return client.CreateNotification(ctx, req)
}

// UpdateNotification client handler
func UpdateNotification(ctx context.Context, req *pb.UpdateNotificationRequest) (*pb.NoResponse, error) {
	conn := Grpc.Dial(os.Getenv("notification_grpc"))
	defer conn.Close()

	client := pb.NewNotificationClient(conn)

	return client.UpdateNotification(ctx, req)
}

// ListNotification client handler
func ListNotification(ctx context.Context, req *pb.ListNotificationRequest) (*pb.ListNotificationResponse, error) {
	conn := Grpc.Dial(os.Getenv("notification_grpc"))
	defer conn.Close()

	client := pb.NewNotificationClient(conn)

	return client.ListNotification(ctx, req)
}

// GetNotification client handler
func GetNotification(ctx context.Context, req *pb.GetNotificationRequest) (*pb.GetNotificationResponse, error) {
	conn := Grpc.Dial(os.Getenv("notification_grpc"))
	defer conn.Close()

	client := pb.NewNotificationClient(conn)

	return client.GetNotification(ctx, req)
}
