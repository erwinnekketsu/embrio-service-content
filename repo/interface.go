package repo

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-content.git/entity"
	"gitlab.com/erwinnekketsu/embrio-service-content.git/util"
)

type INews interface {
	CreateUpdateNews(ctx context.Context, req entity.News) error
	GetNews(ctx context.Context, page int, limit int, status int) (data []entity.News, pagintaion *util.Pagination, err error)
	GetNewsById(ctx context.Context, id int) (data entity.News, err error)
}

type IBanner interface {
	CreateUpdateBanner(ctx context.Context, req entity.Banner) error
	GetBanner(ctx context.Context, page int, limit int, status int) (data []entity.Banner, pagintaion *util.Pagination, err error)
	GetBannerById(ctx context.Context, id int) (data entity.Banner, err error)
}

type IWa interface {
	CreateUpdateWa(ctx context.Context, req entity.Wa) error
	GetWa(ctx context.Context, page int, limit int) (data []entity.Wa, pagintaion *util.Pagination, err error)
	GetWaById(ctx context.Context, id int) (data entity.Wa, err error)
	DeleteWa(ctx context.Context, id int64) error
}
