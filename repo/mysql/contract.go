package mysql

import (
	"context"

	"github.com/jmoiron/sqlx"
	"gitlab.com/erwinnekketsu/embrio-service-content.git/util"
)

type IMysql interface {
	Get(context.Context, *sqlx.DB, interface{}, *util.Query, string) error
	CreateOrUpdate(context.Context, *sqlx.DB, interface{}, string) (lastId int64, err error)
	FindWithPagination(ctx context.Context, db *sqlx.DB, query *util.Query, pagination *util.Pagination, queryString string) (data []News, paginate *util.Pagination, err error)
	Count(ctx context.Context, db *sqlx.DB, query *util.Query, queryString string) (int32, error)
	FindWithPaginationBanner(ctx context.Context, db *sqlx.DB, query *util.Query, pagination *util.Pagination, queryString string) (data []Banner, paginate *util.Pagination, err error)
	FindWithPaginationWa(ctx context.Context, db *sqlx.DB, query *util.Query, pagination *util.Pagination, queryString string) (data []Wa, paginate *util.Pagination, err error)
}
