package mysql

const (
	QueryCreateNews = `
		INSERT INTO artikelBerita (idAdmin, judulArtikel, isiArtikel, status, createdAt, thumbnail) VALUES (:idAdmin, :judulArtikel, :isiArtikel, :status, :createdAt, :thumbnail);
	`

	QueryUpdateNews = `
		UPDATE artikelBerita SET idAdmin=:idAdmin, judulArtikel=:judulArtikel, isiArtikel=:isiArtikel, status=:status, updatedAt=:updatedAt, thumbnail=:thumbnail WHERE id=:id
	`

	QueryUpdateStatusNews = `
		UPDATE artikelBerita SET status=:status, updatedAt=:updatedAt WHERE id=:id
	`

	QueryGetNews = `
		SELECT id, idAdmin, judulArtikel, isiArtikel, status, createdAt, updatedAt, thumbnail FROM artikelBerita
	`

	QueryCountNews = `
		SELECT count(1) FROM artikelBerita
	`

	QueryCountBanner = `
		SELECT count(1) FROM banner
	`

	QueryGetBanner = `
		SELECT id, idAdmin, nama, image, status, createdAt, updatedAt FROM banner
	`

	QueryUpdateStatusBanner = `
		UPDATE banner SET status=:status, updatedAt=:updatedAt WHERE id=:id
	`

	QueryUpdateBanner = `
		UPDATE banner SET idAdmin=:idAdmin, nama=:nama, image=:image, status=:status, updatedAt=:updatedAt WHERE id=:id
	`

	QueryCreateBanner = `
		INSERT INTO banner (idAdmin, nama, image, status, createdAt) VALUES (:idAdmin, :nama, :image, :status, :createdAt);
	`

	QueryCountWa = `
		SELECT count(1) FROM wa_group
	`

	QueryGetWa = `
		SELECT id, nama, image, link, anggota, createdAt, updatedAt FROM wa_group
	`

	QueryUpdateStatusWa = `
		UPDATE wa_group SET anggota=:anggota, updatedAt=:updatedAt WHERE id=:id
	`

	QueryUpdateWa = `
		UPDATE wa_group SET nama=:nama, image=:image, link=:link, anggota=:anggota, updatedAt=:updatedAt WHERE id=:id
	`

	QueryCreateWa = `
		INSERT INTO wa_group (nama, image, link, anggota, createdAt) VALUES (:nama, :image, :link, :anggota, :createdAt);
	`

	QueryDeleteWa = `
		DELETE FROM wa_group WHERE id = :id
	`
)
