package mysql

import "time"

type News struct {
	ID        int32      `json:"id" db:"id"`
	IdAdmin   int32      `json:"admin_id" db:"idAdmin"`
	Title     string     `json:"title" db:"judulArtikel"`
	Content   string     `json:"content" db:"isiArtikel"`
	Status    int32      `json:"status" db:"status"`
	Thumbnail string     `json:"thumbnail" db:"thumbnail"`
	CreatedAt *time.Time `json:"createdAt" db:"createdAt"`
	UpdatedAt *time.Time `json:"updatedAt" db:"updatedAt"`
}

type Banner struct {
	ID        int32      `json:"id" db:"id"`
	IdAdmin   int32      `json:"admin_id" db:"idAdmin"`
	Name      string     `json:"name" db:"nama"`
	Image     string     `json:"image" db:"image"`
	Status    int32      `json:"status" db:"status"`
	CreatedAt *time.Time `json:"createdAt" db:"createdAt"`
	UpdatedAt *time.Time `json:"updatedAt" db:"updatedAt"`
}

type Wa struct {
	ID        int32      `json:"id" db:"id"`
	Name      string     `json:"name" db:"nama"`
	Image     string     `json:"image" db:"image"`
	Link      string     `json:"link" db:"link"`
	Member    int32      `json:"member" db:"anggota"`
	CreatedAt *time.Time `json:"createdAt" db:"createdAt"`
	UpdatedAt *time.Time `json:"updatedAt" db:"updatedAt"`
}
