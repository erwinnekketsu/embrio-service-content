package repo

import (
	"context"
	"log"

	"github.com/jmoiron/sqlx"
	"gitlab.com/erwinnekketsu/embrio-service-content.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-content.git/entity"
	grpcClient "gitlab.com/erwinnekketsu/embrio-service-content.git/repo/grpc/client"
	"gitlab.com/erwinnekketsu/embrio-service-content.git/repo/mysql"
	"gitlab.com/erwinnekketsu/embrio-service-content.git/util"
)

type News struct {
	builder    *builder.Entity
	builderRpc *builder.Grpc
	iMysql     mysql.IMysql
	embrioDB   *sqlx.DB
}

func NewNews() *News {
	return &News{
		builder:    builder.NewEntity(),
		builderRpc: builder.NewGrpc(),
		iMysql:     mysql.NewClient(),
		embrioDB:   mysql.EmbrioDB,
	}
}

func (p *News) CreateUpdateNews(ctx context.Context, req entity.News) error {
	var query string
	if req.ID == 0 {
		query = mysql.QueryCreateNews
	} else {
		if req.IdAdmin == 0 {
			query = mysql.QueryUpdateStatusNews
		} else {
			query = mysql.QueryUpdateNews
		}
	}

	dataNews := p.builder.CreateUpdateNews(ctx, req)
	_, err := p.iMysql.CreateOrUpdate(ctx, p.embrioDB, dataNews, query)

	if err != nil {
		log.Println(err)
		return err
	}

	dataProductNotif := p.builderRpc.NewsNotifRequest(req)
	_, err = grpcClient.CreateNotification(ctx, &dataProductNotif)
	if err != nil {
		log.Println(err)
		return err
	}
	return nil
}

func (p *News) GetNews(ctx context.Context, page int, limit int, status int) (data []entity.News, pagintaion *util.Pagination, err error) {
	query := &util.Query{}
	if status != 0 {
		query = &util.Query{
			Filter: map[string]interface{}{
				"status$eq": status,
			},
		}
	}

	datas, paginations, err := p.iMysql.FindWithPagination(
		ctx,
		p.embrioDB,
		query,
		util.NewPagination(int32(page), int32(limit)),
		mysql.QueryGetNews,
	)

	if err != nil {
		log.Println(err)
		return data, &util.Pagination{}, err
	}

	for _, v := range datas {
		date := ""
		if v.UpdatedAt != nil {
			date = v.UpdatedAt.String()
		} else {
			date = v.CreatedAt.String()
		}
		dataNews := entity.News{
			ID:        v.ID,
			IdAdmin:   v.IdAdmin,
			Title:     v.Title,
			Content:   v.Content,
			Status:    v.Status,
			Thumbnail: v.Thumbnail,
			Date:      date,
		}
		data = append(data, dataNews)
	}

	return data, paginations, nil
}

func (p *News) GetNewsById(ctx context.Context, id int) (data entity.News, err error) {
	query := &util.Query{
		Filter: map[string]interface{}{
			"id$eq": id,
		},
	}
	var dataNews mysql.News
	err = p.iMysql.Get(
		ctx,
		p.embrioDB,
		&dataNews,
		query,
		mysql.QueryGetNews,
	)

	if err != nil {
		log.Println(err)
		return data, err
	}

	data = p.builder.GetNews(dataNews)

	return data, nil
}
