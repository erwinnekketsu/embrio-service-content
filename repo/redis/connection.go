package redis

import (
	"log"
	"os"

	"github.com/go-redis/redis/v8"
)

var RedisClient *redis.Client

func Init() {
	RedisClient = redis.NewClient(&redis.Options{
		Addr:     os.Getenv("redis_host") + ":" + os.Getenv("redis_port"),
		Password: os.Getenv("redis_password"),
		DB:       0,
	})

	_, err := RedisClient.Ping(RedisClient.Context()).Result()
	if err != nil {
		log.Fatalf("error initiliazing Redis Client: %v\n", err)
	}
	log.Println("⇨ Redis Client Started !!")
}
