package repo

import (
	"context"
	"log"

	"github.com/jmoiron/sqlx"
	"gitlab.com/erwinnekketsu/embrio-service-content.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-content.git/entity"
	"gitlab.com/erwinnekketsu/embrio-service-content.git/repo/mysql"
	"gitlab.com/erwinnekketsu/embrio-service-content.git/util"
)

type Wa struct {
	builder  *builder.Entity
	iMysql   mysql.IMysql
	embrioDB *sqlx.DB
}

func NewWa() *Wa {
	return &Wa{
		builder:  builder.NewEntity(),
		iMysql:   mysql.NewClient(),
		embrioDB: mysql.EmbrioDB,
	}
}

func (p *Wa) CreateUpdateWa(ctx context.Context, req entity.Wa) error {
	var query string
	query = mysql.QueryCreateWa

	dataWa := p.builder.CreateUpdateWa(ctx, req)
	_, err := p.iMysql.CreateOrUpdate(ctx, p.embrioDB, dataWa, query)

	if err != nil {
		log.Println(err)
		return err
	}
	return nil
}

func (p *Wa) GetWa(ctx context.Context, page int, limit int) (data []entity.Wa, pagintaion *util.Pagination, err error) {
	query := &util.Query{}

	datas, paginations, err := p.iMysql.FindWithPaginationWa(
		ctx,
		p.embrioDB,
		query,
		util.NewPagination(int32(page), int32(limit)),
		mysql.QueryGetWa,
	)

	if err != nil {
		log.Println(err)
		return data, &util.Pagination{}, err
	}

	for _, v := range datas {
		dataWa := entity.Wa{
			ID:     v.ID,
			Name:   v.Name,
			Image:  v.Image,
			Link:   v.Link,
			Member: v.Member,
		}
		data = append(data, dataWa)
	}

	return data, paginations, nil
}

func (p *Wa) GetWaById(ctx context.Context, id int) (data entity.Wa, err error) {
	query := &util.Query{
		Filter: map[string]interface{}{
			"id$eq": id,
		},
	}
	var dataWa mysql.Wa
	err = p.iMysql.Get(
		ctx,
		p.embrioDB,
		&dataWa,
		query,
		mysql.QueryGetWa,
	)

	if err != nil {
		log.Println(err)
		return data, err
	}

	data = p.builder.GetWa(dataWa)

	return data, nil
}

func (p *Wa) DeleteWa(ctx context.Context, id int64) error {
	var query string
	query = mysql.QueryDeleteWa
	dataWa := p.builder.DeleteWa(ctx, id)
	_, err := p.iMysql.CreateOrUpdate(ctx, p.embrioDB, dataWa, query)
	log.Println(err)
	if err != nil {
		log.Println(err)
		return err
	}
	return nil
}
