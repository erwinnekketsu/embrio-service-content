package grpc

import (
	"fmt"
	"log"
	"net"
	"os"

	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	"go.elastic.co/apm/module/apmgrpc"
	"google.golang.org/grpc"
	"google.golang.org/grpc/health"
	"google.golang.org/grpc/reflection"

	healthpb "google.golang.org/grpc/health/grpc_health_v1"

	pb "gitlab.com/erwinnekketsu/embrio-service-content.git/transport/grpc/proto/content"
)

// Run handler running grpc
func Run() {
	lis, err := net.Listen("tcp", ":"+os.Getenv("grpc_port"))
	if err != nil {
		log.Fatalf("failed to listen : %v", err)
	} else {
		fmt.Printf(" Connect grpc to port : %s \n", os.Getenv("grpc_port"))
	}

	s := grpc.NewServer(grpc.MaxRecvMsgSize(5533721), grpc.MaxSendMsgSize(5533721), grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(
		apmgrpc.NewUnaryServerInterceptor(apmgrpc.WithRecovery()),
	)))

	hsrv := health.NewServer()
	hsrv.SetServingStatus("", healthpb.HealthCheckResponse_SERVING)
	healthpb.RegisterHealthServer(s, hsrv)

	pb.RegisterContentServer(s, NewGrpcServer())

	reflection.Register(s)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve : %v", err)
	}
}
