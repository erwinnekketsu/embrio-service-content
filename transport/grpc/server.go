package grpc

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-content.git/action"
	"gitlab.com/erwinnekketsu/embrio-service-content.git/builder"
	pb "gitlab.com/erwinnekketsu/embrio-service-content.git/transport/grpc/proto/content"
)

type GrpcServer struct {
	builder *builder.Grpc
}

func (gs *GrpcServer) CreateNews(ctx context.Context, req *pb.CreateNewsRequest) (*pb.NoResponse, error) {
	var resp pb.NoResponse
	err := action.NewCreateNews().Handler(ctx, req)
	if err != nil {
		return &resp, err
	}
	return &resp, nil
}

func (gs *GrpcServer) UpdateNews(ctx context.Context, req *pb.UpdateNewsRequest) (*pb.NoResponse, error) {
	var resp pb.NoResponse
	err := action.NewUpdateNews().Handler(ctx, req)
	if err != nil {
		return &resp, err
	}
	return &resp, nil
}

func (gs *GrpcServer) ListNews(ctx context.Context, req *pb.ListNewsRequest) (*pb.ListNewsResponse, error) {
	var resp pb.ListNewsResponse
	data, pagination, err := action.NewListNews().Handler(ctx, req)
	if err != nil {
		return &resp, err
	}
	resPagination := &pb.Pagination{
		PageSize:     int64(pagination.PageSize),
		CurrentPage:  int64(pagination.CurrentPage),
		TotalPage:    int64(pagination.TotalPage),
		TotalResults: int64(pagination.TotalResult),
	}
	return &pb.ListNewsResponse{
		News:       gs.builder.ListNewsResponse(data),
		Pagination: resPagination,
	}, nil
}

func (gs *GrpcServer) GetNews(ctx context.Context, req *pb.GetNewsRequest) (*pb.GetNewsResponse, error) {
	var resp pb.GetNewsResponse
	data, err := action.NewGetNews().Handler(ctx, req)
	if err != nil {
		return &resp, err
	}
	resp.News = gs.builder.GetNewsResponse(data)
	return &resp, nil
}

func (gs *GrpcServer) CreateBanner(ctx context.Context, req *pb.CreateBannerRequest) (*pb.NoResponse, error) {
	var resp pb.NoResponse
	err := action.NewCreateBanner().Handler(ctx, req)
	if err != nil {
		return &resp, err
	}
	return &resp, nil
}

func (gs *GrpcServer) UpdateBanner(ctx context.Context, req *pb.UpdateBannerRequest) (*pb.NoResponse, error) {
	var resp pb.NoResponse
	err := action.NewUpdateBanner().Handler(ctx, req)
	if err != nil {
		return &resp, err
	}
	return &resp, nil
}

func (gs *GrpcServer) ListBanner(ctx context.Context, req *pb.ListBannerRequest) (*pb.ListBannerResponse, error) {
	var resp pb.ListBannerResponse
	data, pagination, err := action.NewListBanner().Handler(ctx, req)
	if err != nil {
		return &resp, err
	}
	resPagination := &pb.Pagination{
		PageSize:     int64(pagination.PageSize),
		CurrentPage:  int64(pagination.CurrentPage),
		TotalPage:    int64(pagination.TotalPage),
		TotalResults: int64(pagination.TotalResult),
	}
	return &pb.ListBannerResponse{
		Banner:     gs.builder.ListBannerResponse(data),
		Pagination: resPagination,
	}, nil
}

func (gs *GrpcServer) GetBanner(ctx context.Context, req *pb.GetBannerRequest) (*pb.GetBannerResponse, error) {
	var resp pb.GetBannerResponse
	data, err := action.NewGetBanner().Handler(ctx, req)
	if err != nil {
		return &resp, err
	}
	resp.Banner = gs.builder.GetBannerResponse(data)
	return &resp, nil
}

func (gs *GrpcServer) CreateWa(ctx context.Context, req *pb.CreateWaRequest) (*pb.NoResponse, error) {
	var resp pb.NoResponse
	err := action.NewCreateWa().Handler(ctx, req)
	if err != nil {
		return &resp, err
	}
	return &resp, nil
}

func (gs *GrpcServer) UpdateWa(ctx context.Context, req *pb.UpdateWaRequest) (*pb.NoResponse, error) {
	var resp pb.NoResponse
	err := action.NewUpdateWa().Handler(ctx, req)
	if err != nil {
		return &resp, err
	}
	return &resp, nil
}

func (gs *GrpcServer) ListWa(ctx context.Context, req *pb.ListWaRequest) (*pb.ListWaResponse, error) {
	var resp pb.ListWaResponse
	data, pagination, err := action.NewListWa().Handler(ctx, req)
	if err != nil {
		return &resp, err
	}
	resPagination := &pb.Pagination{
		PageSize:     int64(pagination.PageSize),
		CurrentPage:  int64(pagination.CurrentPage),
		TotalPage:    int64(pagination.TotalPage),
		TotalResults: int64(pagination.TotalResult),
	}
	return &pb.ListWaResponse{
		Wa:         gs.builder.ListWaResponse(data),
		Pagination: resPagination,
	}, nil
}

func (gs *GrpcServer) GetWa(ctx context.Context, req *pb.GetWaRequest) (*pb.GetWaResponse, error) {
	var resp pb.GetWaResponse
	data, err := action.NewGetWa().Handler(ctx, req)
	if err != nil {
		return &resp, err
	}
	resp.Wa = gs.builder.GetWaResponse(data)
	return &resp, nil
}

func (gs *GrpcServer) DeleteWa(ctx context.Context, req *pb.DeleteWaRequest) (*pb.NoResponse, error) {
	var resp pb.NoResponse
	err := action.NewDeleteWa().Handler(ctx, req)
	if err != nil {
		return &resp, err
	}
	return &resp, nil
}

func NewGrpcServer() *GrpcServer {
	return &GrpcServer{
		builder: builder.NewGrpc(),
	}
}
