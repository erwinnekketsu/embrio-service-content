package util

import (
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"log"
	"math/big"
	"net/smtp"
	"os"
	"strconv"
	"time"

	grpcMiddleware "github.com/grpc-ecosystem/go-grpc-middleware"
	"golang.org/x/crypto/bcrypt"
	"google.golang.org/grpc"
)

const (
	DateFormatRFC3339 = time.RFC3339
)

var StatusCodeSuccess = []string{"200", "201", "202"}
var StatusClientSlice = []string{"capture", "settlement", "expire", "pending"}

var BankName = "BRI"

func GetRedisKey(domain, id string) string {
	return domain + ":" + id
}

func FormatDateToRFC3339(t time.Time) string {
	return t.Format(DateFormatRFC3339)
}

func StringToInt(s string) int {
	i, _ := strconv.Atoi(s)
	return i
}

func StringToInt64(s string) int64 {
	i, _ := strconv.ParseInt(s, 10, 64)
	return i
}

func EncodeBase64(s string) string {
	return base64.StdEncoding.EncodeToString([]byte(s))
}

func BcryptPassword(s string) string {
	passoword := []byte(s)

	hashedPassword, err := bcrypt.GenerateFromPassword(passoword, bcrypt.DefaultCost)
	if err != nil {
		return ""
	}
	return string(hashedPassword)
}

func GenerateToken(email string) string {
	emailBytes := []byte(email)

	hash, err := bcrypt.GenerateFromPassword(emailBytes, bcrypt.DefaultCost)
	if err != nil {
		log.Println(err)
		return ""
	}

	return base64.StdEncoding.EncodeToString(hash)
}

func SendMail(email string, token string) {

	// Sender data.
	from := os.Getenv("email_from")
	password := os.Getenv("email_password")

	// Receiver email address.
	to := []string{
		email,
	}

	// smtp server configuration.
	smtpHost := os.Getenv("smtp_host")
	smtpPort := os.Getenv("smtp_port")

	link := os.Getenv("utl_host_verify") + token
	// Message.
	// message := []byte("This is click this link for account verification." + link)
	message := []byte("To: " + email + "\r\n" +
		"Subject: Please verify your account?\r\n" +
		"\r\n" +
		"This is click this link for account verification " + link + ".\r\n")

	// Authentication.
	auth := smtp.PlainAuth("", from, password, smtpHost)

	// Sending email.
	err := smtp.SendMail(smtpHost+":"+smtpPort, auth, from, to, message)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("Email Sent Successfully!")
}

func GenerateRandomString(n int) (string, error) {
	const letters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-"
	ret := make([]byte, n)
	for i := 0; i < n; i++ {
		num, err := rand.Int(rand.Reader, big.NewInt(int64(len(letters))))
		if err != nil {
			return "", err
		}
		ret[i] = letters[num.Int64()]
	}

	return string(ret), nil
}

// Dial: grpc server with new relic or elastic apm middleware ,
func Dial(addr string, opts ...grpc.UnaryClientInterceptor) *grpc.ClientConn {
	conn, err := grpc.Dial(
		addr,
		grpc.WithInsecure(),
		grpc.WithUnaryInterceptor(grpcMiddleware.ChainUnaryClient(opts...)),
	)

	if err != nil {
		log.Fatal("could not connect to", addr, err)
	}
	return conn
}
